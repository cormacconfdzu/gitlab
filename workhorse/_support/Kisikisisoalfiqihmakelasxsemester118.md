## Kisikisisoalfiqihmakelasxsemester118

 
  
 
**LINK > [https://www.google.com/url?q=https%3A%2F%2Fgeags.com%2F2tDc6K&sa=D&sntz=1&usg=AOvVaw3ySGZzmjzlcspJglLZIpHa](https://www.google.com/url?q=https%3A%2F%2Fgeags.com%2F2tDc6K&sa=D&sntz=1&usg=AOvVaw3ySGZzmjzlcspJglLZIpHa)**

 
 
 
 
 
# Kisikisisoalfiqihmakelasxsemester118: A Guide to Islamic Jurisprudence for Tenth Grade Students
 
Kisikisisoalfiqihmakelasxsemester118 is a keyword that refers to a study guide for Islamic jurisprudence (fiqih) for tenth grade students of Madrasah Aliyah (Islamic senior high school) in Indonesia. Fiqih is the science of Islamic law that covers various aspects of worship and social life. Fiqih is derived from the sources of Islamic law, such as the Quran, the Sunnah, the consensus of scholars (ijma'), and analogical reasoning (qiyas).
 
In this article, we will provide an overview of the topics covered in fiqih for tenth grade students in the first semester. The topics are based on the curriculum issued by the Ministry of Religious Affairs of Indonesia in 2020. The topics include:
 
- The concept of fiqih and its development in Islam
- The purification (taharah) and its problems
- The menstruation (haid), irregular bleeding (istihadlah), and postnatal bleeding (nifas)
- The prayer (shalat) and its rules
- The congregational prayer (shalat jamaah), the Friday prayer (shalat jum'ah), and the traveler's prayer (shalat musafir)
- The funeral rites (pemulasaraan jenazah) and its wisdom
- The alms (zakat) and its wisdom

Each topic will be explained in detail with the definitions, terms, conditions, rules, examples, and exercises. The aim of this study guide is to help students understand and practice fiqih in accordance with the teachings of Islam.
 
We hope that this article will be useful for students who want to learn more about fiqih and prepare for their exams. We also hope that this article will inspire students to appreciate the richness and diversity of Islamic jurisprudence.
  
## The Concept of Fiqih and Its Development in Islam
 
The word fiqih comes from the Arabic root fa-qa-ha, which means to understand, to comprehend, or to know. Fiqih is the science of Islamic law that covers various aspects of worship and social life. Fiqih is derived from the sources of Islamic law, such as the Quran, the Sunnah, the consensus of scholars (ijma'), and analogical reasoning (qiyas).
 
The development of fiqih can be divided into four periods: the period of Prophet Muhammad (peace be upon him), the period of the companions (sahabah), the period of codification (tadwin), and the period of imitation (taqlid). In each period, fiqih faced different challenges and opportunities that shaped its characteristics and diversity.
 
The period of Prophet Muhammad (peace be upon him) was the time when fiqih was revealed and practiced by the Prophet and his followers. The main source of fiqih in this period was the Quran and the Sunnah. The Quran is the word of God that was revealed to Prophet Muhammad through Angel Gabriel. The Sunnah is the sayings, actions, and approvals of Prophet Muhammad that were recorded by his companions. The Quran and the Sunnah are the primary sources of Islamic law that provide guidance and rulings for Muslims.
 
The period of the companions (sahabah) was the time when fiqih was transmitted and applied by the companions of Prophet Muhammad after his death. The main source of fiqih in this period was still the Quran and the Sunnah, but also the consensus of scholars (ijma') and analogical reasoning (qiyas). Ijma' is the agreement of qualified scholars on a certain issue based on evidence from the Quran and the Sunnah. Qiyas is the extension of a ruling from an existing case to a new case that has a similar effective cause (illah). Ijma' and qiyas are secondary sources of Islamic law that help to deal with new situations and issues that arise in different contexts.
 
The period of codification (tadwin) was the time when fiqih was compiled and systematized by jurists (fuqaha) into books and schools of thought (madhhab). The main source of fiqih in this period was still the Quran and the Sunnah, but also ijma', qiyas, and other methods such as public interest (maslahah), custom (urf), presumption of continuity (istishab), blocking the means (sadd al-dhara'i'), etc. The most prominent jurists in this period were Imam Abu Hanifah, Imam Malik, Imam Ash-Shafi'i, and Imam Ahmad bin Hanbal, who founded the four major schools of Sunni fiqih: Hanafi, Maliki, Shafi'i, and Hanbali. Each school has its own methodology and interpretation of Islamic law that reflects its historical and geographical background.
 
The period of imitation (taqlid) was the time when fiqih was followed and adopted by Muslims without questioning or reasoning. The main source of fiqih in this period was the opinions and verdicts of jurists from different schools of thought. Taqlid means to follow or imitate a jurist or a school without knowing or understanding their evidence or arguments. Taqlid became widespread among Muslims due to various factors such as political stability, social conformity, intellectual stagnation, etc. Taqlid has its advantages and disadvantages for Muslim society.
 dde7e20689
 
 
